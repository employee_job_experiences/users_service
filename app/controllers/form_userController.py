from app.models.form_userModel import Form_User, Session, engine, Base
from app.models.usersModel import Users
from werkzeug.security import generate_password_hash, check_password_hash
from flask import jsonify, request
from flask_jwt_extended import *
import os,json,datetime
from hashlib import pbkdf2_hmac

Base.metadata.create_all(engine)
session = Session()


#show users join with form users data
@jwt_required()
def showJoinUserForm():
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    try:
        if (user_id[0]['role']=='admin'): 
            data_dict['code'] = 200
            data_dict['success'] = True
            for f, u in session.query(Form_User, Users).join(Users, Form_User.user_id == Users.id).all():
                col = ['form_user_id','user_id','fullname','email','address','division','position','year_submission']
                val = f.id,f.user_id,u.fullname,u.email,f.address,f.division,f.position,f.year_submission
                res = dict(zip(col,val))
                res_list.append(res)
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        else:
            data_dict['code'] = 200
            data_dict['success'] = True
            for f, u in session.query(Form_User, Users).join(Users, Form_User.user_id == Users.id).filter(Form_User.user_id == '{}'.format(user_id[0]['id'])):
                col = ['form_user_id','user_id','fullname','email','address','division','position','year_submission']
                val = f.id,f.user_id,u.fullname,u.email,f.address,f.division,f.position,f.year_submission
                res = dict(zip(col,val))
                res_list.append(res)
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
    except:
        data_dict['code'] = 401
        data_dict['success'] = False
        data_dict['message'] = "Data error"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 401
        return result
   
#show all form users
@jwt_required()
def showFormUser():
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    if (user_id[0]['role']=='admin'): 
        data = session.query(Form_User).all()
        try:
            data_dict['code'] = 200
            data_dict['success'] = True
            for row in data:
                col = ['id','user_id','address','division','position','year_submission']
                val = row.id,row.user_id,row.address,row.division,row.position,row.year_submission
                res = dict(zip(col,val))
                res_list.append(res)
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        except:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "Data error"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result
    else:
        data_dict['code'] = 403
        data_dict['success'] = False
        data_dict['message'] = "Permission not allowed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 403
        return result

#show all form user by user_id
@jwt_required()
def showFormUserbyUserId(userid):
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    data = session.query(Form_User).filter(Form_User.user_id == '{}'.format(userid))
    # return jsonify(user_id)
    if (user_id[0]['role']=='admin'): 
        data_dict['code'] = 200
        data_dict['success'] = True
        for row in data:
            col = ['id','user_id','address','division','position','year_submission']
            val = row.id,row.user_id,row.address,row.division,row.position,row.year_submission
            res = dict(zip(col,val))
            res_list.append(res)
        if res_list:
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        else:
            data_dict['data'] = res_list
            data_dict['message'] = "Data not found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
    else:
        if userid == user_id[0]['id']:
            data_dict['code'] = 200
            data_dict['success'] = True
            for row in data:
                col = ['id','user_id','address','division','position','year_submission']
                val = row.id,row.user_id,row.address,row.division,row.position,row.year_submission
                res = dict(zip(col,val))
                res_list.append(res)
            if res_list:
                data_dict['data'] = res_list
                data_dict['message'] = "Data found"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result
            else:
                data_dict['data'] = res_list
                data_dict['message'] = "Data not found"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result

        else:
            data_dict['code'] = 403
            data_dict['success'] = False
            data_dict['message'] = "Permission not allowed"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 403
            return result

#insert new form user
def insertFormUser(**params):
    final_res = []
    data_dict = {}
    try:
        data_dict['code'] = 200
        data_dict['success'] = True
        data = Form_User(params['user_id'],params['address'],params['division'],params['position'],params['year_submission'])
        session.add(data)
        session.commit()
        data_dict['message'] = "Form User created successfully"
        final_res.append(data_dict)
        return jsonify(final_res)
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Create Form User failed"
        final_res.append(data_dict)
        return jsonify(final_res)

#update data form user
@jwt_required()
def updateFormUserById(userid,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Form_User).filter(Form_User.user_id == '{}'.format(userid),Form_User.id == '{}'.format(params['id'])).first()
    try:
        if (user_id[0]['role']=='admin'):    
            if params['address'] and params['division'] and params['position'] and params['year_submission']:
                data_dict['code'] = 200
                data_dict['success'] = True
                data.address = params['address']
                data.division = params['division']
                data.position = params['position']
                data.year_submission = params['year_submission']
                if data.address:
                    session.commit()
                    data_dict['message'] = "User updated successfully"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result
                else:
                    data_dict['code'] = 200
                    data_dict['success'] = True
                    data_dict['message'] = "User not found"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result

            else:
                data_dict['code'] = 400
                data_dict['success'] = False
                data_dict['message'] = "All datas must be filled"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 400
            return result
        else:
            if userid == user_id[0]['id']:
                if params['address'] and params['division'] and params['position'] and params['year_submission']:
                    data_dict['code'] = 200
                    data_dict['success'] = True
                    data.address = params['address']
                    data.division = params['division']
                    data.position = params['position']
                    data.year_submission = params['year_submission']
                    if data.address:
                        session.commit()
                        data_dict['message'] = "User updated successfully"
                        final_res.append(data_dict)
                        result = jsonify(final_res)
                        result.status_code = 200
                        return result
                    else:
                        data_dict['code'] = 200
                        data_dict['success'] = True
                        data_dict['message'] = "User not found"
                        final_res.append(data_dict)
                        result = jsonify(final_res)
                        result.status_code = 200
                        return result
                else:
                    data_dict['code'] = 400
                    data_dict['success'] = False
                    data_dict['message'] = "All datas must be filled"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 400
                    return result

            else:
                data_dict['code'] = 401
                data_dict['success'] = False
                data_dict['message'] = "You can only edit your own account"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 401
                return result
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Failed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 400
        return result  


#delete data form user by id
@jwt_required()
def deleteFormUserById(userid,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Form_User).filter(Form_User.user_id == '{}'.format(userid),Form_User.id == '{}'.format(params['id'])).first()
    try:
        if (user_id[0]['role']=='admin'):  
            session.delete(data)
            session.commit()
            data_dict['message'] = "Form User deleted successfully"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result

        else:
            if userid == user_id[0]['id']:
                session.delete(data)
                session.commit()
                data_dict['message'] = "Form User deleted successfully"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result
            else:
                data_dict['code'] = 401
                data_dict['success'] = False
                data_dict['message'] = "You can only delete your own account"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 401
                return result
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Failed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 400
        return result







   