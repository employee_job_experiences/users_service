# USER_SERVICE
Ini merupakan aplikasi manajemen pengalaman kerja pegawai di suatu perusahaan atau instansi dimana data ini dapat digunakan untuk membantu mengolah data kepegawaian.

Untuk User Service ini berisi aplikasi untuk management user login dan pendaftaran form user. Di sini saya juga menyertakan file dump database mysql saya.

Untuk Dokumentasi API pada service ini yaitu :

# Manajemen User :

<details><summary>Click to expand</summary>

- **POST Create User** 

Digunakan untuk membuat user baru, adapun data role merupakan tipe data enum yang mempunyai value "admin" dan "user"

endpoint URL : http://127.0.0.1:5000/user/create

Body raw (json)

```
{
    "fullname":"<nama>",
    "email":"<email>",
    "password":"<password>",
    "username":"<username>",
    "role":"<admin | user>"
}
```

- **POST Login User** 

Digunakan untuk login user yang telah dibuat sebelumnya.

endpoint URL : http://127.0.0.1:5000/user/login

Body raw (json)

```
{
    "username":"<username>",
    "password":"<password>"
}
```

- **GET All Users** 

Untuk menampilkan semua list users yang terdaftar, pada endpoint ini hanya role admin yang hanya bisa melihat datanya.

endpoint URL : http://127.0.0.1:5000/users

**Authorization** : Bearer Token


- **GET All Users by Id** 

Untuk menampilkan list user berdasarkan id, pada endpoint ini hanya role admin yang bisa melihat semua datanya.

endpoint URL : http://127.0.0.1:5000/user/{id}

**Authorization** : Bearer Token


- **POST Update User** 

Untuk melakukan perubahan data user, data yang bisa diubah yaitu fullname dan email. Role admin bisa merubah semua data user, sedangkan role user hanya bisa mengubah data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/user/update/{id}

**Authorization** : Bearer Token

Body raw (json)

```
{
    "fullname":"<fullname>",
    "email":"<email>"
}
```

- **DEL Delete User**

Untuk menghapus data user. Role admin bisa menghapus semua data user, sedangkan role user hanya bisa menghapus data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/user/delete/{id}

**Authorization** : Bearer Token

</details>


# Manajemen Form User :

<details><summary>Click to expand</summary>


Manajemen Form User ini merupakan form pendaftaran awal yang berisi detail user.

- **POST Create Form User**

Untuk menginputkan data user dalam form pendaftaran awal, **user_id** diisikan dengan no id dari user login

endpoint URL : http://127.0.0.1:5000/form_user/create

Body raw (json)
```
{
    "user_id":"<user_id>",
    "address":"<address>",
    "division":"<division>",
    "position":"<position>",
    "year_submission":"<year:YYYY>"
}
```

- **GET All Form Users Data** 

Untuk menampilkan semua data form users yang sudah diinput, pada endpoint ini hanya role admin yang hanya bisa melihat datanya.

endpoint URL : http://127.0.0.1:5000/form_user

**Authorization** : Bearer Token


- **GET Form Users Data By User Id** 

Untuk menampilkan semua data form users berdasarkan user id, pada endpoint ini role admin bisa melihat semua data sedangkan user hanya bisa melihat data yang telah diinput oleh dirinya sendiri.

endpoint URL : http://127.0.0.1:5000/form_user/{user_id}

**Authorization** : Bearer Token



- **POST Update Form User** 

Untuk melakukan perubahan data form user yang telah diinputkan berdasarkan user id, data yang bisa diubah yaitu address, division, position dan year_submission. Role admin bisa merubah semua data form user, sedangkan role user hanya bisa mengubah data milik dia sendiri. Pada kolom body raw json ditambahkan **{id}** yang merupakan id dari form_user_register yang akan diupdate.

endpoint URL : http://127.0.0.1:5000/form_user/update/{user_id}

**Authorization** : Bearer Token

Body raw (json)

```
{
   {
    "id":<id>,
    "address": "<address",
    "division": "<division>",
    "position": "<position>",
    "year_submission": "<year:YYYY>"
}
}
```

- **DEL Delete Form User**

Untuk menghapus data form user. Role admin bisa menghapus semua data form user, sedangkan role user hanya bisa menghapus data form user milik dia sendiri. Pada kolom body raw json ditambahkan **{id}** yang merupakan id dari form_user_register yang akan dihapus.

endpoint URL : http://127.0.0.1:5000/form_user/delete/{user_id}

**Authorization** : Bearer Token

Body raw (json)

```
{
    "id":<id>
}
```



- **GET User Combined** 

Untuk menampilkan hasil join data form_user_register dengan data users. Role admin bisa menampilkan semua data, sedangkan role user hanya menampilkan data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/form_user_combined

**Authorization** : Bearer Token

</details>

![ER Diagram](https://gitlab.com/employee_job_experiences/job_experiences_service/-/raw/main/ERD_Diagram.png)
